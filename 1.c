#include <sys/types.h> // pid_t
#include <unistd.h> // fork, sleep
#include <sys/wait.h> // wait
#include <stdio.h>
#include <stdlib.h> // EXIT_FAILURE

int path[3],depth;

void spawn(int n)
{	
	
	depth++;
	for (int i = 0;i < n;i++){
		path[depth] = i;
		pid_t child;
		child = fork();
		if(child < 0){
			fputs("Error while fork\n",stderr);
			exit(EXIT_FAILURE);
		}else if(child == 0){
			//child
			for(int a = 0;a < 10;a++)
				printf("Process %d is executed, my father is %d\n",getpid(),getppid());
			sleep(1);
			
			//we add a flush because when stdout is redirected through 
			//pipe the output is fully buffered instead of line buffered
			fflush(stdout);
			
			if(depth == 1)
				if(path[depth] == 0)
					spawn(2);
				else
					spawn(1);
			exit(0);
		}
	}
	depth--;
	while(wait(NULL) > 0);
}

int main(void)
{
	spawn(2);
}
