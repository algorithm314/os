#include <stdio.h>
#include <unistd.h> 
#include <stdlib.h> 
#include <signal.h>
#include <sys/wait.h>

int n;
pid_t children[6];
char **argv2;
	
void handler(){
	int i = 1;
	while(1){
		printf("Child %d %d is executed (%d)\n", n, getpid(), i);
		i++;
		sleep(1);
	}
}

void h2(){
	for(int i=0;i<4;i++){
		for(int j=1;j<6;j++){
			if(i==0)
				kill(children[atoi(argv2[j])], SIGUSR1);
			else
				kill(children[atoi(argv2[j])], SIGCONT);
			sleep(3);
			if( i == 4)
				kill(children[atoi(argv2[j])], SIGKILL);
			else
				kill(children[atoi(argv2[j])], SIGSTOP);
		}
	}
	exit(0);
}

int is_checked[6];

int main(int argc, char **argv){
	if(argc != 6){
			fputs("Error: Invalid input arguments\n",stderr);
			exit(EXIT_FAILURE);
	}
	argv2 = argv;
	for(int i=1;i<6;i++){
		int arg = atoi(argv[i]);
		if (arg > 5 || arg < 1){
			fputs("Error: Invalid input arguments\n",stderr);
			exit(EXIT_FAILURE);
		}
		// avoid having the same argument twice
		if (is_checked[arg] == 0)
			is_checked[arg] = 1;
		else{
			fputs("Error: Invalid input arguments\n",stderr);
			exit(EXIT_FAILURE);
		}
	}
	signal(SIGUSR2,h2);
	signal(SIGUSR1, handler);
	for(n=1;n<6;n++){
		children[n] = fork();
		if(children[n] < 0){
			fputs("Error while fork\n",stderr);
			exit(EXIT_FAILURE);
		}
		else if(children[n] == 0){
			// child
			
			if(n == 5)
				kill(getppid(),SIGUSR2);
			pause();
			exit(0);

		}
	}
	// parent
	//sleep(1);
	
	pause();
}
