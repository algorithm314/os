#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int critical_section[3][6];
int fd[2];
int value;

void semaphore_wait(){
	// stall execution until semaphore_signal writes into the pipe
	read(fd[0], &value, sizeof(value));
}

void semaphore_signal(){
	// add an integer into the pipe
	write(fd[1], &value, sizeof(value));
}

int main(){
	pid_t child[3];
	critical_section[0][0] = critical_section[0][3] = critical_section[0][5] = 1;
	critical_section[1][1] = critical_section[1][3] = critical_section[1][5] = 1;
	critical_section[2][0] = critical_section[2][2] = critical_section[2][4] = 1;

	if(pipe(fd) == -1){
		fputs("ERROR while creating pipe\n",stderr);
		return EXIT_FAILURE;
	}
	
	// add an integer into the pipe
	write(fd[1], &value, sizeof(value));
	
	// create the 3 children	
	for(int i = 0; i < 3; i++){
		child[i] = fork();
		if(child[i] < 0){
			fputs("Error while fork\n",stderr);
			exit(EXIT_FAILURE);
		}
		else if(child[i] == 0){ 
			// child with number i
			for(int section = 0; section < 6; section++){
				// execute critical section
				if(critical_section[i][section]){
					// wait until some other process ends its critical section
					semaphore_wait();
					for(int a = 0; a < 5; a++){
						printf("Child%d %d executes a critical section\n", i+1, getpid());
						sleep(1);
					}
					// send signal to other process to enter its critical section
					semaphore_signal();
				}
				// execute non critical section
				else
					for(int a = 0; a < 7; a++){
						printf("Child%d %d executes a non critical section\n", i+1, getpid());
						sleep(1);
					}
			}
			exit(0);
		}
	}
	// wait all children to terminate
	wait(NULL);
	wait(NULL);
	wait(NULL);
}
