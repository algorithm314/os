#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>

int main(int argc, char **argv)
{
	if(argc != 3){
		fputs("ERROR: The program accepts 2 arguments\n",stderr);
		return EXIT_FAILURE;
	}

	int n = atoi(argv[1]);
	int k = atoi(argv[2]);
	pid_t child[10];
	if(n > 10 || n < 1 || k < 0){
		fputs("ERROR: 1 <= n <= 10 and k >= 0\n",stderr);
		return EXIT_FAILURE;
	}
		
	int fd[10][2];
	int id;
	long long int fact,num;

	for(int a = 0;a < n;a++)
		if(pipe(fd[a]) == -1){
			fputs("ERROR while creating pipe\n",stderr);
			return EXIT_FAILURE;
		}
	//read id write id+1
	fact = 1;num = 0; // 0! = 1
	write(fd[0][1],&fact,sizeof(fact));
	write(fd[0][1],&num,sizeof(num));
	
	for(id = 0;id < n;id++){
		if((child[id] = fork()) < 0){
			fputs("Error while fork\n",stderr);
			exit(EXIT_FAILURE);
		}else if(child[id] == 0){
			//child
			while(1){
				read(fd[id][0],&fact,sizeof(fact));
				read(fd[id][0],&num,sizeof(num));
				if(num == k){
					printf("%lld\n",fact);
					return 0;
				}
				num++;
				fact = fact*num;
				write(fd[(id+1)%n][1],&fact,sizeof(fact));
				write(fd[(id+1)%n][1],&num,sizeof(num));
			}
		}
	}
	
	//kill(-getpgrp(),SIGINT);
	
	//or this is better
	pid_t final = wait(NULL);
	for(id = 0;id < n;id++)
		if(child[id] != final)
			kill(child[id],SIGTERM);
	return 0;
}
